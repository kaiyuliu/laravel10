<?php

namespace App\Providers;

use App\Models\Topic;
use App\Observers\TestObserve;
use App\Observers\TopicObserve;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends Serviceprovider
{
    /**
     * the event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        registered::class => [
            sendemailverificationnotification::class,
        ],

    ];

    protected $observers = [
//        Topic::class => TopicObserve::class,// 这样也可以
        Topic::class => [TopicObserve::class],
//        Topic::class => [TopicObserve::class, TestObserve::class],// ^ 可以这么做，两个observe都能生效，但似乎这样意义不大
    ];

    /**
     * register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * 确定是否应用自动发现事件和监听器。
     * determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }

    /**
     * 获取应用于发现事件的监听器目录。
     *
     * @return array<int, string>
     */
    protected function discoverEventsWithin(): array
    {
        return [
            $this->app->path('Listeners'),
        ];
    }
}
