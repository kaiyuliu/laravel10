<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function __construct()
    {
        /*
        这里额外注意了：官方文档样例中只除外了『login』
        这样的结果是，token 只能在有效期以内进行刷新，过期无法刷新
        // 如果把 refresh 也放进去，token 即使过期但仍在刷新期以内也可刷新
        // 不过刷新一次作废
        // 另外关于上面的中间件，官方文档写的是『auth:api』
        // 但是我推荐用 『jwt.auth』，效果是一样的，但是有更加丰富的报错信息返回
        */
        $this->middleware('jwt.auth', ['except' => ['login', 'register', 'refresh']]);
    }

    /**
     * @throws ValidationException
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|phone:CN,mobile|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails()) {
//            return response()->json($validator->errors(), 400);
            return $this->fail($validator->errors(), 400);
        }

        return User::query()->create($validator->validated()); //create函数里面会将密码进行加密
    }

    public function login()
    {
        ['username' => $username, 'password' => $password] = request(['username', 'password']);// php关联数组解包
//       $username = request('username'); //获取请求数据的几种写法
//        $password = request()->password;
        filter_var($username, FILTER_VALIDATE_EMAIL) ?
            $credentials['email'] = $username :
            $credentials['phone'] = $username;
        $credentials['password'] = $password;
        if (!$token = auth('api')->attempt($credentials)) {
            /*          attempt内部最终验证逻辑是 password_verify这个函数，所以密码参加验证时不需要加密
                        password_verify('123456', password_hash('123456',PASSWORD_BCRYPT)) // true
                        password_verify('123456', bcrypt('123456')) // true
            */
            return $this->fail('账号或密码错误', 401);
        }
        return $this->respondWithToken($token);
    }

    public function respondWithToken($token): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
//            'user' => auth('api')->user(),
        ]);
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth('api')->logout();
        return $this->ok('退出成功');
    }

    public function refresh()
    {
        return $this->respondWithToken(token: auth('api')->refresh());
    }

    public function me(): \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\JsonResource
    {
        return $this->success(auth('api')->user());
    }
}
