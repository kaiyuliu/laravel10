<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    public function index(Link $link)
    {
        return $link->ordered('desc')->get();
//        return $link->query()->orderBy('order', 'desc')->get();
    }

    public function show(Link $link): Link
    {
//        $link->moveToStart();
//        $link->moveOrderDown();
        Link::setNewOrder([5, 1,  4],10);
        return $link;
    }
}
