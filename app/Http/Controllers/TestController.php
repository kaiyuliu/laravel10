<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(): void
    {
        $data = $this->createRange(10);
        foreach ($data as $value){
            sleep(1);
            echo $value.PHP_EOL;
        }
    }

    public function createRange($num)
    {
        $data = [];
        for ($i = 0; $i < $num; $i++){
//            $data[] = time();
            yield time();
        }
        return $data;
    }
}
