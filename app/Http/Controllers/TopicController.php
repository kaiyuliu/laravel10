<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;
use Predis\Command\Argument\Server\To;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class TopicController extends Controller
{
    public function index(Topic $topic)
    {
//        dump($data = $topic->query()->find(1));
//        return $data;
//        return response()->json($topic->query()->find(1));
//        前端只需传page=?就可以实现页数跳转
//        return response()->json($topic->query()->paginate());
        //topic?filter[title]=consequatur&sort=-title,-body&include=replies&fields[replies]=id,content,topic_id&fields[topics]=id,title,body
        return QueryBuilder::for(Topic::class)
            ->allowedFields( 'id', 'title', 'body',
                'replies.id','replies.content', 'replies.topic_id')
            ->allowedFilters(['title', 'body']) //topic?filter[title]=consequatur&filter[body]=necessitatibus
//            ->allowedFilters([AllowedFilter::exact('title')])
//            ->defaultSort('id')
            ->allowedSorts('title', 'body')
//            ->with('replies')
            ->allowedIncludes(['replies'])
            ->withCount('replies')
            ->withExists('replies')
            ->paginate(3);
    }

    public function store(Topic $topic): Topic
    {
        $topic->title = request('title');
        $topic->body = request('body');
        $topic->user_id = request('user_id');
        $topic->category_id = request('category_id');
        $topic->save();
        return $topic;
    }
}
