<?php

namespace App\Http\Controllers;

use App\ModelFilters\UserFilter;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{
    public function index(Request $request): \Illuminate\Database\Eloquent\Collection|array
    {
//        Redis::set('name', 'Taylor');

//        echo 'hello world';
//        $query = User::query();
//        if ($request->has('name'))
//        {
//            $query->where('name', 'LIKE', '%' . $request->input('name') . '%');
//        }
//        return $query->get();
        return User::filter($request->all())->get();
    }

    public function show(User $user): User
    {
//        echo  Redis::get('name');
        visits($user)->increment();
        return $user;
    }

}
