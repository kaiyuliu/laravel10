<?php
namespace App\Traits;
use App\Enums\ApiCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

trait ApiResponse
{
    /**
     * @var int
     */
    protected int $statusCode = ApiCode::HTTP_OK;

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode): static
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param $data
     * @param array $header
     * @return JsonResponse
     */
    public function respond($data, array $header = []): JsonResponse
    {
        return Response::json($data, $this->getStatusCode(), $header);
    }

    /**
     * @param $status
     * @param array $data
     * @param null $code
     * @return JsonResponse
     */
    public function status($status, array $data, $code = null): JsonResponse
    {
        if ($code) {
            $this->setStatusCode($code);
        }

        $status = [
            'status' => $status,
            'code' => $this->statusCode
        ];

        $data = array_merge($status, $data);
        return $this->respond($data);

    }

    /**
     * @param $message
     * @param int $code
     * @return JsonResponse
     */
    public function failed($message, int $code = ApiCode::BAD_REQUEST): JsonResponse
    {
        return $this->status('error', [
            'message' => $message,
            'code' => $code
        ]);
    }

    /**
     * @param $message
     * @param string $status
     * @return JsonResponse
     */
    public function message($message, string $status = "success"): JsonResponse
    {
        return $this->status($status, [
            'message' => $message
        ]);
    }

    /**
     * @param $data
     * @param string $status
     * @return JsonResponse
     */
    public function success($data, string $status = "success"): JsonResponse
    {
        return $this->status($status, compact('data'));
    }
}
