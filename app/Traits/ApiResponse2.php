<?php

namespace App\Traits;
trait ApiResponse2
{
    /**
     * 成功返回.
     *
     * @param array  $data
     * @param string $msg
     *
     * @return mixed
     */
    public function success($data, $msg = "OK")
    {
        $this->parseNull($data);
        $result = [
            "code" => 200,
            "msg" => $msg,
            "data" => $data,
        ];

        return response()->json($result, 200);
    }

    /**
     * 失败返回.
     *
     * @param string $code
     * @param array  $data
     * @param string $msg
     *
     * @return mixed
     */
    public function error($code = "422", $data = [], $msg = "fail")
    {
        $result = [
            "code" => $code,
            "msg" => $msg,
            "data" => $data,
        ];

        return response()->json($result, 200);
    }

    /**
     * @param $filePath
     */
    public function download($filePath)
    {
        header('Content-Encoding: UTF-8');
        header("Content-type:application/octet-stream");
        header("Content-Disposition:attachment;filename=".basename($filePath));
        header("Accept-ranges:bytes");
        readfile($filePath);//读取文件
        unlink($filePath);
        exit;
    }

    /**
     * 如果返回的数据中有 null 则那其值修改为空 （安卓和IOS 对null型的数据不友好，会报错）.
     *
     * @param $data
     */
    private function parseNull(&$data)
    {
        if (is_array($data)) {
            foreach ($data as &$v) {
                $this->parseNull($v);
            }
        } else {
            if (null === $data) {
                $data = "";
            }
        }
    }

}
