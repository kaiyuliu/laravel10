<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, Filterable;

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    protected Log $log;

//    public function __construct(Log $log)
//    {
//        parent::__construct();
//        $this->log = $log;
//    }

    public function login(): void
    {
        echo 'login success...';
        $this->log->write();

    }

    public function modelFilter(): ?string
    {
        return $this->provideFilter(\App\ModelFilters\UserFilter::class);
    }

//    与上面定义 的 cats 'password' => 'hashed' 作用一样
//    protected function password(): Attribute
//    {
////        return Attribute::make(
//////            get: fn ($value) => Hash::make($value),
////            set: fn ($value) => bcrypt($value)
////        );
////        return Attribute::set(fn ($value) => bcrypt($value));
//        return Attribute::set(function ($value) {
//            // 如果值的长度等于 60，即认为是已经做过加密的情况 不等于 60，做密码加密处理
//            if (strlen($value) != 60) {
//                $value = bcrypt($value);
//            }
//            return $value;
//        });
//    }
}
