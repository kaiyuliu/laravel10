<?php

namespace App\Models;

//use Illuminate\Support\Facades\Cache;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;


/**
 * @method remember(int $cache_expire_in_minutes)
 * @method flushCache()
 */
class Link extends Model implements Sortable
{
    use SortableTrait;

    public array $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];
    protected $fillable = ['title', 'link'];

//    public string $cache_key = 'larabbs_links';
    public string $rememberCacheTag  = 'larabbs_links';
    protected int $cache_expire_in_minutes = 1440;

    public function getAllCached()
    {
//        return Cache::remember($this->cache_key, $this->cache_expire_in_minutes, function () {
//            return $this->all();
//        });
        return $this->remember($this->cache_expire_in_minutes)->get();
    }
}
