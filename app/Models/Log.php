<?php
namespace App\Models;

interface Log
{
    public function write();
}
