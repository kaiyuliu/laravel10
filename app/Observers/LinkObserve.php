<?php

namespace App\Observers;

use App\Models\Link;
use Illuminate\Support\Facades\Cache;

class LinkObserve
{

    public function saved(Link $link): void
    {
//        Cache::forget($link->cache_key);
        $link->flushCache();
    }


}
