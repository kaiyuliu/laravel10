<?php

namespace App\Observers;

use App\Models\Topic;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

class TestObserve
{

    public function creating(Topic $topic): void
    {
        Mail::raw("topic: {$topic->id} creating", function (Message $message){
            $message->subject('Test Creating');
            $message->to('wu@qq.com');
        });

    }
    /**
     * Handle the Topic "created" event.
     */
    public function created(Topic $topic): void
    {
        Mail::raw("topic: {$topic->id} created", function (Message $message){
            $message->subject('Test Created');
            $message->to('wu@qq.com');
        });
    }

    /**
     * Handle the Topic "updated" event.
     */
    public function updated(Topic $topic): void
    {
        //
    }

    /**
     * Handle the Topic "deleted" event.
     */
    public function deleted(Topic $topic): void
    {
        //
    }

    /**
     * Handle the Topic "restored" event.
     */
    public function restored(Topic $topic): void
    {
        //
    }

    /**
     * Handle the Topic "force deleted" event.
     */
    public function forceDeleted(Topic $topic): void
    {
        //
    }
}
