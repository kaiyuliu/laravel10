<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Summer',
                'phone' => NULL,
                'email' => 'summer@example.com',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => '7F3WuIQjtb',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/ZqM7iaP4CR.png',
                'introduction' => 'Placeat dolor inventore placeat eius.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Prof. Oliver Oberbrunner II',
                'phone' => NULL,
                'email' => 'treva.purdy@example.org',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => 'biCyVJtEOG',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/ZqM7iaP4CR.png',
                'introduction' => 'Cum vel voluptatem iste quas consequatur officia.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Luz Olson Jr.',
                'phone' => NULL,
                'email' => 'qkuhlman@example.net',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => 'P68WAAYeKI',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/LOnMrqbHJn.png',
                'introduction' => 'Quia minus tempora eligendi ratione quaerat.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Brett Marks',
                'phone' => NULL,
                'email' => 'albert06@example.net',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => 'CT43s4nDWb',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/ZqM7iaP4CR.png',
                'introduction' => 'Est aspernatur voluptates assumenda.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Era Jones V',
                'phone' => NULL,
                'email' => 'joel19@example.org',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => 'BgFmmzUUlI',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/xAuDMxteQy.png',
                'introduction' => 'Rerum molestiae laudantium quae velit.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Dr. Cheyanne O\'Keefe IV',
                'phone' => NULL,
                'email' => 'cole.zelma@example.net',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => 'ortlvoBH4m',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/Lhd1SHqu86.png',
                'introduction' => 'Id et suscipit ipsa occaecati cumque vero sit.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Marcella Greenfelder I',
                'phone' => NULL,
                'email' => 'skiles.christelle@example.com',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => 'vdww2Y82zH',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/NDnzMutoxX.png',
                'introduction' => 'Dolor rerum veritatis ut tenetur assumenda quis.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Prof. Taylor Cartwright DVM',
                'phone' => NULL,
                'email' => 'mackenzie60@example.org',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => 'dHXkHfZbaF',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/Lhd1SHqu86.png',
                'introduction' => 'Doloremque laborum aut consequatur quia.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Prof. Tyshawn Blick II',
                'phone' => NULL,
                'email' => 'tobin.vonrueden@example.net',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => 'y9r7OAiexO',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/NDnzMutoxX.png',
                'introduction' => 'Occaecati earum numquam error natus quo necessitatibus.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Maurice Becker',
                'phone' => NULL,
                'email' => 'qfeil@example.com',
                'email_verified_at' => '2023-06-12 13:26:41',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'weixin_openid' => NULL,
                'weixin_unionid' => NULL,
                'remember_token' => 'qAkibfiH0F',
                'created_at' => '2023-06-12 13:26:41',
                'updated_at' => '2023-06-12 13:26:41',
                'avatar' => 'https://cdn.learnku.com/uploads/images/201710/14/1/Lhd1SHqu86.png',
                'introduction' => 'Voluptas magni corrupti rerum ullam odit nulla.',
                'notification_count' => 0,
                'last_actived_at' => NULL,
            ),
        ));
        
        
    }
}