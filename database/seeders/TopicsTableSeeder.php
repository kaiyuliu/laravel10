<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TopicsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('topics')->delete();

        DB::table('topics')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => 'Voluptatum facilis esse aspernatur.',
                'body' => 'Ipsam sint reprehenderit facere sint sit. Eaque commodi adipisci fuga accusantium odio nemo omnis. Id quos dolor mollitia dolorem laudantium.',
                'user_id' => 5,
                'category_id' => 4,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Voluptatum facilis esse aspernatur.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
            1 =>
            array (
                'id' => 2,
                'title' => 'Non quam aut laborum fugiat.',
                'body' => 'Illo atque voluptas veniam maiores voluptatem. Voluptas accusamus odio aliquid quia aut facilis sunt consequatur. Ut numquam qui repellendus nam aut sed.',
                'user_id' => 9,
                'category_id' => 1,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Non quam aut laborum fugiat.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
            2 =>
            array (
                'id' => 3,
                'title' => 'Nulla architecto est est iusto dolor quaerat.',
                'body' => 'Numquam modi distinctio voluptas reprehenderit deleniti non. Expedita saepe quia voluptate temporibus ut possimus repellendus. In at et eaque nemo velit maiores magnam.',
                'user_id' => 8,
                'category_id' => 3,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Nulla architecto est est iusto dolor quaerat.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
            3 =>
            array (
                'id' => 4,
                'title' => 'Et earum aspernatur autem quia voluptas ullam quia voluptatem.',
                'body' => 'Ea aperiam deleniti tenetur libero explicabo voluptatem eum. Modi aspernatur aliquid itaque vero quia. Neque velit pariatur eveniet nostrum nesciunt deserunt.',
                'user_id' => 2,
                'category_id' => 4,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Et earum aspernatur autem quia voluptas ullam quia voluptatem.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
            4 =>
            array (
                'id' => 5,
                'title' => 'Cupiditate voluptatem ut quia eveniet corporis blanditiis magni.',
                'body' => 'Voluptate et unde illum sapiente animi sit at. Voluptatem ipsum accusamus iste voluptatem. Cupiditate et eos vel incidunt. Alias eum quis sit natus eos.',
                'user_id' => 6,
                'category_id' => 2,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Cupiditate voluptatem ut quia eveniet corporis blanditiis magni.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
            5 =>
            array (
                'id' => 6,
                'title' => 'Debitis eum aliquam nesciunt veritatis quia magnam libero.',
                'body' => 'Quidem odit sit sed voluptatibus. Ad voluptas a aut et aspernatur. Quia ab aut recusandae sint qui ea expedita voluptates.',
                'user_id' => 4,
                'category_id' => 1,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Debitis eum aliquam nesciunt veritatis quia magnam libero.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
            6 =>
            array (
                'id' => 7,
                'title' => 'Voluptatem voluptates ab voluptatem ut voluptatem dolores.',
                'body' => 'Repudiandae molestias sequi esse deleniti. Quasi libero sint accusamus fugiat. Voluptatem et amet quia soluta qui. Non ab aperiam odit consequatur non aut.',
                'user_id' => 4,
                'category_id' => 2,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Voluptatem voluptates ab voluptatem ut voluptatem dolores.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
            7 =>
            array (
                'id' => 8,
                'title' => 'Odit voluptas aliquid sequi amet nesciunt.',
                'body' => 'Qui corrupti eveniet at et quaerat atque. Ut quibusdam voluptas exercitationem incidunt aspernatur. Quisquam sapiente et velit eligendi. Enim natus consequatur error alias.',
                'user_id' => 8,
                'category_id' => 2,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Odit voluptas aliquid sequi amet nesciunt.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
            8 =>
            array (
                'id' => 9,
                'title' => 'Reiciendis nihil aut impedit in error deserunt.',
                'body' => 'Eligendi quibusdam et eos. Laboriosam in ut asperiores dolor aut atque. Repudiandae fuga quis vel saepe. Pariatur veritatis aut fugiat et corrupti.',
                'user_id' => 4,
                'category_id' => 3,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Reiciendis nihil aut impedit in error deserunt.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
            9 =>
            array (
                'id' => 10,
                'title' => 'Praesentium et repudiandae laborum libero.',
                'body' => 'Tenetur ab est libero et unde eum. Non et sed nihil itaque sit omnis dolorum. Sunt voluptates doloribus dolor. Unde esse sunt adipisci.',
                'user_id' => 9,
                'category_id' => 1,
                'reply_count' => 0,
                'view_count' => 0,
                'last_reply_user_id' => 0,
                'order' => 0,
                'excerpt' => 'Praesentium et repudiandae laborum libero.',
                'slug' => NULL,
                'created_at' => '2023-06-12 13:26:42',
                'updated_at' => '2023-06-12 13:26:42',
            ),
        ));


    }
}
