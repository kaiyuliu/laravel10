<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('links')->delete();

        DB::table('links')->insert(array (
            0 =>
            array (
                'id' => 1,
                'order' => 11,
                'title' => 'Miss Rosalind Altenwerth Sr.',
                'link' => 'http://www.rath.com/',
                'created_at' => '2023-06-12 13:26:44',
                'updated_at' => '2023-10-30 08:41:50',
            ),
            1 =>
            array (
                'id' => 2,
                'order' => 3,
                'title' => 'Annamarie Kuphal',
                'link' => 'http://www.cremin.com/ea-totam-eos-et-ea-ut-laboriosam-quia',
                'created_at' => '2023-06-12 13:26:44',
                'updated_at' => '2023-10-30 08:40:22',
            ),
            2 =>
            array (
                'id' => 3,
                'order' => 4,
                'title' => 'Bernadette Simonis',
                'link' => 'http://www.wyman.info/nisi-magnam-beatae-consequatur-veritatis-accusantium-et',
                'created_at' => '2023-06-12 13:26:44',
                'updated_at' => '2023-10-30 08:40:22',
            ),
            3 =>
            array (
                'id' => 4,
                'order' => 12,
                'title' => 'Carmel Smitham',
                'link' => 'http://www.mueller.com/et-ut-quia-molestias-fugiat',
                'created_at' => '2023-06-12 13:26:44',
                'updated_at' => '2023-10-30 08:41:50',
            ),
            4 =>
            array (
                'id' => 5,
                'order' => 10,
                'title' => 'Orland Zulauf V',
                'link' => 'http://www.nader.com/',
                'created_at' => '2023-06-12 13:26:44',
                'updated_at' => '2023-10-30 08:41:50',
            ),
            5 =>
            array (
                'id' => 6,
                'order' => 7,
                'title' => 'Prof. Bernhard Bergstrom MD',
                'link' => 'http://pollich.com/ipsam-est-tempore-ut-quia-aliquid-recusandae.html',
                'created_at' => '2023-06-12 13:26:44',
                'updated_at' => '2023-10-30 08:36:37',
            ),
        ));


    }
}
