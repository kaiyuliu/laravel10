<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '分享',
                'description' => '分享创造，分享发现',
                'post_count' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => '教程',
                'description' => '开发技巧、推荐扩展包等',
                'post_count' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => '问答',
                'description' => '请保持友善，互帮互助',
                'post_count' => 0,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => '公告',
                'description' => '站点公告',
                'post_count' => 0,
            ),
        ));
        
        
    }
}