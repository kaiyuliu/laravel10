<?php

use App\Http\Controllers\ExcelController;
use App\Models\User;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
//    phpinfo();
    $a = 1;
    $b = 2;
    $c = $a + $b;
    \Illuminate\Support\Facades\Auth::attempt();
    return view('welcome');
});


Route::get('file', function () {
    return __FUNCTION__;
});

// Route::group(['prefix'=>'test'], function () {
//     Route::get('demo1', function (){
//         $data = ['a', 'b', 'c'];
//         foreach ($data as $k => $v){
//             $v = &$data[$k];
//             dump($data);
//         }
//         var_dump($data);
//     });
//     $x = null;
//     dump($x);
//     --$x;      // $x is NULL
//     dump($x);
//     $x--;      // still NULL
//     dump($x);
//     $x -= 1;   // $x is -1
//     dump($x);
//     $x = null;
//     ++$x;      // $ix is 1
//     dump($x);
//     $a = true;
//     $a++;
//     echo $a.'<pre/>';

//     $input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
//     $rand_keys = array_rand($input, 2);
//     var_dump($rand_keys);
//     echo $input[$rand_keys[0]] . "\n";
//     echo $input[$rand_keys[1]] . "\n";
// });

// Route::resource('user', UserController::class);
// // Route::get('user',[UserController::class, 'index']);

// Route::get('sum', function (){
//     $arr = [1,2,3,4,5];
//     $sum = array_reduce($arr, function ($carry, $item){
//         return $carry + $item;
//     });
//     dd($sum);
// });

Route::get('ref', function () {
    // 获取User的reflectionClass对象
    $reflector = new ReflectionClass(User::class);

    // 拿到User的构造函数
    $constructor = $reflector->getConstructor();

    // 拿到User的构造函数的所有依赖参数
    $dependencies = $constructor->getParameters();


    // 创建user对象
    $user = $reflector->newInstance();

    // 创建user对象，需要传递参数的
    $user = $reflector->newInstanceArgs($dependencies = []);
});
Route::get('yield', [\App\Http\Controllers\TestController::class, 'index']);
Route::get('db', function () {
    return \Illuminate\Support\Facades\DB::table('users')->find(1);
//    return  \DB::table('users')->find(1);// 可行 在没有命名空间的文件下可以直接DB
});

Route::get('user', [UserController::class, 'index']);
Route::get('count/{user}', function (User $user) {
    return visits($user)->count();
});
Route::get('user/{user}', [UserController::class, 'show']);
Route::get('ip', function () {
    return geoip(request()->ip())->toArray();
});

Route::get('excel', function () {
    return \Maatwebsite\Excel\Facades\Excel::download(new \App\Exports\UsersExport(), 'users.xlsx');
});

Route::get('links', function (\App\Models\Link $link) {
//    logger('logger log');
//    clock('clock log');// 不会写入日志文件
    clock()->event('link-index', 'link请求数据')->color('purple')->begin();;
    $links = $link->getAllCached();
    clock()->event('link-index')->end();

    return $links;
});
Route::get('dump', function () {
    phpinfo();
    dump('hello world');
    dump(request()->all());
    return view('welcome');
});
Route::resource('topic', \App\Http\Controllers\TopicController::class)->withoutMiddleware(\App\Http\Middleware\VerifyCsrfToken::class);
//Route::get('topic', [\App\Http\Controllers\TopicController::class, 'index']);
//Route::post('topic', [\App\Http\Controllers\TopicController::class, 'store']);
Route::get('links2', [\App\Http\Controllers\LinkController::class, 'index']);
Route::get('links2/{link}', [\App\Http\Controllers\LinkController::class, 'show']);

Route::get('mail', function () {
    Mail::raw('topic creating', function (Message $message) {
//        dd($message);
        $message->subject('topic');
        $message->to('wu@qq.com');
    });
    Mail::html('<h2>hello mail</h2>', function (Message $message) {
        $message->subject('love');
        $message->to('shuang@qq.com');
    });
});
Route::get('excel', [ExcelController::class, 'index']);
Route::post('excel', [ExcelController::class, 'edit']);
Route::get('excel/export', [ExcelController::class, 'export']);
